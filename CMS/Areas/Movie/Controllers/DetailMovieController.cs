﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using CMS.Repository;

namespace CMS.Areas.Movie.Controllers
{
    public class DetailMovieController : Controller
    {
        private readonly IMovieRepository repository;

        public DetailMovieController(IMovieRepository objIrepository)
        {
            repository = objIrepository;
        }

        private const string viewName = "~/Views/Movie/Detail.cshtml";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = repository.GetById(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(viewName, movie);
        }
    }
}