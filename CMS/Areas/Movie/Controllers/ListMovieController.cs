﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using CMS.Repository;

namespace CMS.Areas.Movie.Controllers
{
    public class ListMovieController : Controller
    {
        private readonly IMovieRepository repository;

        public ListMovieController(IMovieRepository objIrepository)
        {
            repository = objIrepository;
        }

        private const string viewName = "~/Views/Movie/List.cshtml";

        public ActionResult Index()
        {
            return View(viewName, repository.GetAll);
        }

        
        public ActionResult DeleteConfirmed(int id)
        {
            repository.Delete(id);
            repository.Save();
            return RedirectToAction("index", "listmovie");
        }
    }
}