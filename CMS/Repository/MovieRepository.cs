﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.Areas.Movie;
using CMS.Models;
using CMS.Core;

namespace CMS.Repository
{
    public class MovieRepository : IMovieRepository
    {
        CMSContext context;
        public MovieRepository(CMSContext context)
        {
            this.context = context;
        }

        
        public IQueryable<Movie> GetAll
        {
            get { return context.Movies; }
        }

        public void Delete(int id)
        {
            var movie = context.Movies.Find(id);
            context.Movies.Remove(movie);
        }

        public Movie GetById(int? id)
        {
            Movie objMovie = new Movie();
            objMovie = context.Movies.Where(p => p.Id == id).FirstOrDefault();
            return objMovie;
        }

        public void InsertOrUpdate(Movie movie)
        {
            if (movie.Id == default(int))
            {
                // New entity
                context.Movies.Add(movie);
            }
            else
            {
                // Existing entity
                context.Entry(movie).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
    public interface IMovieRepository : IDisposable
    {
        IQueryable<Movie> GetAll { get; }
        Movie GetById(int? id);
        void InsertOrUpdate(Movie movie);
        void Delete(int id);
        void Save();        
    }
}