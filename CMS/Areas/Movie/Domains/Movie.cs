﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Areas.Movie
{
    public class Movie
    {
        public Movie()
        {

        }
        public Int64 Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }
    }
}