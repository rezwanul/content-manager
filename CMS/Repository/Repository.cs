﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Linq.Expressions;
using CMS.Repository;
using CMS.Core;

namespace CMS.Repository
{
    public abstract class Repository<T> :
    IRepository<T> where T : class 
    {
        private readonly IDBContext _context;
        private IDbSet<T> _entities;

        public Repository(IDBContext context)
        {
            this._context = context;
        }

        
        public virtual IQueryable<T> Table
        {
            get
            {
                return this.Entities;
            }
        }

        private IDbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = _context.Set<T>();
                }
                return _entities;
            }
        }
    }

    public interface IRepository<T> where T : class
    {       
        IQueryable<T> Table { get; }
    }
}