﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CMS.Areas.Movie;

namespace CMS.Core
{
    public class CMSContext : DbContext
    {
        public CMSContext()
            : base("DefaultConnection")
        {
        }
        
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}