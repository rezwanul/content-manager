namespace CMS.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CMS.Core;

    internal sealed class Configuration : DbMigrationsConfiguration<CMSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CMSContext context)
        {
           
        }
    }
}
