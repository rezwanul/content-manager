﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using CMS.Repository;

namespace CMS.Areas.Movie.Controllers
{
    public class EditMovieController : Controller
    {
        private readonly IMovieRepository repository;

        public EditMovieController(IMovieRepository objIrepository)
        {
            repository = objIrepository;
        }

        private const string viewName = "~/Views/Movie/Edit.cshtml";
        // GET: Movie/EditMovie
        public ActionResult Index()
        {
            return View(viewName);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie move = repository.GetById(id);
            if (move == null)
            {
                return HttpNotFound();
            }
            return View(viewName, move);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Movie movie)
        {
            if (ModelState.IsValid)
            {
                repository.InsertOrUpdate(movie);
                repository.Save();                
            }
            return RedirectToAction("index", "listmovie");
        }
    }
}