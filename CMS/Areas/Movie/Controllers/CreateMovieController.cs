﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CMS.Repository;



namespace CMS.Areas.Movie.Controllers
{    
    public class CreateMovieController : Controller
    {
        private readonly IMovieRepository repository;

        public CreateMovieController(IMovieRepository objIrepository)
        {
            repository = objIrepository;
        }

        private const string viewName = "~/Views/Movie/Create.cshtml";

        public ActionResult Index()
        {
            return View(viewName);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Movie movie)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    repository.InsertOrUpdate(movie);
                    repository.Save();
                }
                catch
                {
                    //some error log
                }
                
                //return RedirectToAction("/listmovie/index");
            }

            return RedirectToAction("index","listmovie");
        }
    }
}